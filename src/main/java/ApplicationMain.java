import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@ComponentScan(basePackages = "controllers")
public class ApplicationMain {


    public static void main(String[] args) {
        SpringApplication.run(ApplicationMain.class,args);
    }
}
