package controllers;

import jwtutils.JWTUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
public class AccountController {

    @RequestMapping("loginvalidate")
    @ResponseBody
    public Object loginValidate(String username,String password){
        HashMap<Object, Object> msg = new HashMap<>();
        if (username.equals("admin")&&password.equals("123")){
            msg.put("code",0);
            msg.put("entity",JWTUtils.createJWT(username));
        }else{
            msg.put("code",-1);
        }
        return msg;
    }

}
