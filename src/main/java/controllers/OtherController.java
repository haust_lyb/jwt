package controllers;

import jwtutils.JWTUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Controller
public class OtherController {

    @RequestMapping("others")
    @ResponseBody
    public Object test(HttpServletRequest request){
        String tocken = request.getHeader("jwt");
        HashMap<Object, Object> map = new HashMap<>();
        String result = JWTUtils.parseJWT(tocken);
        if (result==null){
            map.put("msg","签名失败");
        }else{
            map.put("msg",result);
        }

        return map;
    }
}
