package jwtutils;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.Scanner;

public class JWTUtils {
    private static final String KEY1 = "somethinghereshouldbelongsomethinghereshouldbelongsomethinghereshouldbelongsomethinghereshouldbelongsomethinghereshouldbelongsomethinghereshouldbelong";

    /**
     * 由字符串生成加密key
     *
     * @return
     */
    private static SecretKey generalKey() {
        byte[] keyBytes = KEY1.getBytes();
        SecretKey key = Keys.hmacShaKeyFor(keyBytes);
        return key;
    }

    /**
     * 创建jwt
     *
     * @param subject
     * @return
     * @throws Exception
     */
    public static String createJWT(String subject) {

        SecretKey key = generalKey();
        Date now = new Date();
        Date thirtyMinutes = new Date(System.currentTimeMillis() + 15 * 1000);//15秒后过期

        String jws = Jwts.builder()
                .setSubject(subject) // 主题
                .setIssuedAt(now) // 签发时间
                .setExpiration(thirtyMinutes) // 过期时间
                .signWith(key)
                .compact();

        return jws;

    }

    /**
     * 解密jwt
     *
     * @param jwt 密钥
     * @return 主题
     * @throws Exception 如果发生 JwtException，说明该密钥无效
     */
    public static String parseJWT(String jwt) throws JwtException {
        SecretKey key = generalKey();

        try {
            return Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(jwt)
                    .getBody()
                    .getSubject();
        } catch (JwtException ex) {
            System.out.println("签证失效");
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(JWTUtils.createJWT("ceshi"));
        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            String tocken = scanner.nextLine();
            System.out.println(JWTUtils.parseJWT(tocken));
        }

    }
}


